const sdk = require("node-appwrite");
const fetch = require('node-fetch')

module.exports = async function (req, res) {

  const client = new sdk.Client();

  let users = new sdk.Users(client);

  if (
    !req.env['APPWRITE_FUNCTION_ENDPOINT'] ||
    !req.env['APPWRITE_FUNCTION_API_KEY']
  ) {
    console.warn("Environment variables are not set. Function cannot use Appwrite SDK.");
  } else {
    client
      .setEndpoint(req.env['APPWRITE_FUNCTION_ENDPOINT'])
      .setProject(req.env['APPWRITE_FUNCTION_PROJECT_ID'])
      .setKey(req.env['APPWRITE_FUNCTION_API_KEY'])
  }

  if (!req.payload) return res.json({ code: 'no-payload' }, '400')

  const payload = JSON.parse(req.payload)

  let userPrefs;
  try {
    userPrefs = await users.getPrefs(payload.userId)
    userPrefs.suspended = true
  } catch (e) {
    return res.json({
      code: 'error-getuserprefs',
      error: e,
    }, '400');
  }

  try {
    const options = {
      method: 'POST',
      headers: {
        Authorization: 'Basic WGVvdmFseXRlOmtNKjhuRXMzNTchalJlXm1KYnZrRSFOIw==',
        'content-type': 'application/json'
      },
      body: JSON.stringify({ discordId: userPrefs.discordId, time: payload.time, reason: payload.reason })
    };

    const response = await fetch('http://192.168.1.176:7289/suspend', options)

  } catch (e) {
    return res.json({
      code: 'error-discord',
      error: e,
    }, '400')
  }

  try {
    await users.updatePrefs(payload.userId, userPrefs)
  } catch (e) {
    res.json({
      code: 'error-updateprefs',
      error: e
    }, 400)
  }

  res.json({
    status: 'succes'
  })
};
