const sdk = require("node-appwrite");

module.exports = async function (req, res) {
  const client = new sdk.Client();

  let database = new sdk.Databases(client, '62f76f33db1de0fd1f46');
  let teams = new sdk.Teams(client);

  if (!req.payload) return res.json({ code: 'no-payload' }, '400')

  const payload = JSON.parse(req.payload)

  if (
    !req.env['APPWRITE_FUNCTION_ENDPOINT'] ||
    !req.env['APPWRITE_FUNCTION_API_KEY']
  ) {
    console.warn("Environment variables are not set. Function cannot use Appwrite SDK.");
  } else {
    client
      .setEndpoint(req.env['APPWRITE_FUNCTION_ENDPOINT'])
      .setProject(req.env['APPWRITE_FUNCTION_PROJECT_ID'])
      .setJWT(payload.jwt)
      .setSelfSigned(true);
  }

  let newTeam;
  let newDatabase;

  try {
    newTeam = await teams.create('unique()', payload.teamName)
  } catch (e) {
    return res.json({ code: 'error-createteam', error: e }, '500');
  }

  try {
    newDatabase = await database.createDocument('teams', newTeam.$id, {teamName: payload.teamName, teamColor: payload.teamColor, members: [payload.userData.$id]}, [`team:${newTeam.$id}`, 'role:member'], [`team:${newTeam.$id}`])
  } catch (e) {
    return res.json({ code: 'error-createdocument', error: e }, '500')
  }

  res.json({
    status: 'succes'
  })
};
